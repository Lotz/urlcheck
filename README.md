 _____________________________________________
/ This repository has been moved to codeberg. \
\ https://codeberg.org/ManfredLotz/urlcheck   /
 ---------------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

# urlcheck

The `urlcheck` command line utility checks (http, https, ftp) links found in text files.

# Installation

`bin/urlcheck` is a 64-bit statically linked binary without further dependencies. 
Just download it and run it.

# urlcheck options

Running `urlcheck --help` gives

 > urlcheck 1.0.0-beta
 > Manfred Lotz <manfred@ctan.org>
 > Checks URLs found in text files ending in `.md`, `.txt` or `.org` or file README if existing.
 >
 > USAGE:
 >     urlcheck [FLAGS] [OPTIONS] [text_files]...
 >
 > FLAGS:
 >     -h, --help         Prints help information
 >         --no-colors    Don't display messages in color
 >     -a, --all          Print all statuses
 >     -V, --version      Prints version information
 >
 > OPTIONS:
 >     -d, --directory <directory>    Check links in all text files in directory tree given
 >
 > ARGS:
 >     <text_files>...    Checks text files specified


# License 

Licensed under either of

-    Apache License, Version 2.0, (LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0)
-    MIT license (LICENSE-MIT or http://opensource.org/licenses/MIT)

at your option.
