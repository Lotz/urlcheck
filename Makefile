

STRIP:
	strip -s target/x86_64-unknown-linux-musl/release/urlcheck

REPOSITORY_BIN:
	install -s target/x86_64-unknown-linux-musl/release/urlcheck bin/

LOCAL_INSTALL:
	install -s target/x86_64-unknown-linux-musl/release/urlcheck ~/bin/

UPLOAD_TO_COMEDY:
	scp bin/urlcheck manfred@comedy.dante.de:~/bin
