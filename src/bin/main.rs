use once_cell::sync::Lazy; // 1.3.1
use std::io::{self}; // required for io::stdout()
#[macro_use]
mod utils;
use utils::*;

use urlcheck::LinkCheck;

use std::str;

extern crate linkify;

use clap::{Command, CommandFactory, Parser, ValueHint};
use clap_complete::{generate, Generator, Shell};
use std::os::unix::fs::FileTypeExt;
use std::process;

#[cfg(unix)]
use walkdir::{DirEntry, WalkDir};

static ARGS: Lazy<Args> = Lazy::new(Args::parse);

#[derive(Parser, Debug, PartialEq)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(long = "no-colors", help = "Don't display messages in color")]
    no_colors: bool,
    #[clap(short = 'a', long = "all", help = "Print all statuses")]
    print_all: bool,
    #[clap(
        short = 'd',
        long = "directory",
        help = "Check links in all text files in directory tree given",
        group = "only_one",
        value_hint = ValueHint::DirPath,
    )]
    directory: Option<String>,
    #[clap(help = "Checks text files specified", group = "only_one")]
    text_files: Vec<String>,
    #[clap(long = "generate-completion", group = "only_one", arg_enum)]
    generator: Option<Shell>,
}

fn print_completions<G: Generator>(gen: G, cmd: &mut Command) {
    generate(gen, cmd, cmd.get_name().to_string(), &mut io::stdout());
}

fn main() {
    if let Some(generator) = ARGS.generator {
        let mut cmd = Args::command();
        println!("Generating completion file for {:?}...", generator);
        print_completions(generator, &mut cmd);
        process::exit(0)
    }
    let lc = LinkCheck::new(5 * num_cpus::get(), ARGS.print_all, ARGS.no_colors);
    for fname in &ARGS.text_files {
        lc.check_urls(fname);
    }

    if let Some(dir) = &ARGS.directory {
        if !exists_dir(dir) {
            println!("Directory {} does not exist. Exiting...", &dir);
            process::exit(1);
        };
        check_directory_tree(dir, &lc);
    }
}

fn check_directory_tree(d: &str, lc: &LinkCheck) {
    for dir_entry in WalkDir::new(d).follow_links(false) {
        match dir_entry {
            Ok(dir_entry) => {
                let dir_entry_str = match dir_entry.path().to_str() {
                    Some(d) => d,
                    None => continue,
                };

                let ft = get_filetype(&dir_entry);

                if ft != FType::Regular || ft == FType::Symlink {
                    continue;
                }

                if filename(dir_entry_str) == Some("README")
                    || dir_entry_str.ends_with(".org")
                    || dir_entry_str.ends_with(".md")
                    || dir_entry_str.ends_with(".txt")
                {
                    lc.check_urls(dir_entry_str);
                }
            }
            Err(e) => {
                eprintln!("{}", e);
            }
        };
    }
}

//#[derive(Debug, PartialEq, Eq)]
#[derive(Debug, PartialEq, Eq)]
pub enum FType {
    Regular,
    Directory,
    Symlink,
    BlockDevice,
    CharDevice,
    Fifo,
    Socket,
    Error(String),
}

fn get_filetype(entry: &DirEntry) -> FType {
    match entry.metadata() {
        Ok(mt) => {
            let ft = mt.file_type();
            if ft.is_symlink() {
                return FType::Symlink;
            }
            if ft.is_dir() {
                return FType::Directory;
            }
            if ft.is_block_device() {
                return FType::BlockDevice;
            }
            if ft.is_char_device() {
                return FType::CharDevice;
            }
            if ft.is_fifo() {
                return FType::Fifo;
            }
            if ft.is_socket() {
                return FType::Socket;
            }
            FType::Regular
        }
        Err(e) => FType::Error(format!("{}", e)),
    }
}
