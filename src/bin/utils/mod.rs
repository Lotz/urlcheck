extern crate walkdir;

use std::fs;

// returns true if file is a directory and does exist
// returns false otherwise
pub fn exists_dir(file: &str) -> bool {
    match fs::metadata(file) {
        Ok(attr) => attr.is_dir(),
        Err(_) => false,
    }
}

// #[test]
// fn test_dirname() {
//     assert!(dirname("/etc/fstab") == Some("/etc"));
//     assert!(dirname("/etc/") == Some("/etc"));
//     assert!(dirname("/") == Some("/"));
// }
//
// pub fn _dirname(entry: &str) -> Option<&str> {
//     if let Some(stripped) = entry.strip_suffix('/') {
//         if entry.len() == 1 {
//             return Some(entry);
//         }
//         return Some(stripped);
//     }
//
//     let pos = entry.rfind('/');
//     match pos {
//         None => None,
//         Some(pos) => Some(&entry[..pos]),
//     }
// }

#[test]
fn test_filename() {
    assert!(filename("/etc/fstab") == Some("fstab"));
    assert!(filename("fstab") == Some("fstab"));
    assert!(filename("../pkgcheck.rs/testdirs/fira.tds.zip") == Some("fira.tds.zip"));
    assert!(filename("/etc/") == None);
    assert!(filename("/") == None);
}

pub fn filename(entry: &str) -> Option<&str> {
    if entry.ends_with('/') {
        return None;
    }

    let pos = entry.rfind('/');
    match pos {
        None => Some(entry),
        Some(pos) => Some(&entry[pos + 1..]),
    }
}
